<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,"home"]);
Route::get('/register',[AuthController::class,"register"]);
Route::post('/kirim',[AuthController::class,"kirim"]);

Route::get('/master',function(){
    return view('layout.master');
});
Route::get('/table',[HomeController::class,"table"]);
Route::get('/data-table',[HomeController::class,"datatable"]);
Route::get('/cast/create',[CastController::class,"creates"]);
Route::post('/masukin',[CastController::class,"masukin"]);
Route::get('/cast',[CastController::class,"read"]);

Route::get('/cast/{id}',[CastController::class,"detail"]);

//update
Route::get('/cast/{id}/edit',[CastController::class,"update"]);
Route::put('/cast/{id}',[CastController::class,"edit"]);

//delete
Route::delete('/cast/{id}',[CastController::class,"destroy"]);