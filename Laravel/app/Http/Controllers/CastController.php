<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function creates(){
        return view('cast.create');
    }
    public function masukin(Request $request){
        $request->validate([
            'inputnama' => 'required',
            'inputumur' => 'required',
            'inputbio' => 'required'
        ]);
        //cara lain 
        /*
        $request->validate([
            'inputnama' => 'required',
            'inputumur' => 'required',
            'inputbio' => 'required'
        ],[
            'inputnama.required'=>'nama cast harus diisi!',
            'inputumur.required'=>'umur cast harus diisi!',
            'inputbio.required'=>'bio cast harus diisi!',
            'inputbio.min'=>'bio cast harus diisi min 8 karakter',
        ]);
        */
        DB::table('cast')->insert([
            'nama'=> $request['inputnama'],
            'umur'=> $request['inputumur'],
            'bio'=> $request['inputbio']
        ]);
        return redirect('/cast/create'); //pinda ke create lagi
    }
    public function read(){
        $casts = DB::table('cast')->get();

        //cara 1
        //return view('index',['cast'=>$casts]);

        //cara 2
        return view('cast.index',compact('casts'));
    }
    public function detail($id)
    {
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.detail',compact('cast'));
    }
    public function update($id){
        $cast = DB::table('cast')->where('id',$id)->first();
        return view('cast.update',compact('cast'));
    }
    public function edit(Request $request, $id)
    {
        $request->validate([
            'inputnama' => 'required',
            'inputumur' => 'required',
            'inputbio' => 'required'
        ]);
        DB::table('cast')
        ->where('id',$id)
        ->update([
            'nama' => $request['inputnama'],
            'umur' => $request['inputumur'],
            'bio' => $request['inputbio']
        ]);
        return redirect('/cast');
    }
    public function destroy($id)
    {
        DB::table('cast')->where('id',$id)->delete();
        return redirect('/cast');
    }
}
