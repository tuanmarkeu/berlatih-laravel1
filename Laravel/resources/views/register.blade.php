@extends('layout.master')
@section('judul')
    Buat Account Baru!
@endsection
@section('content')
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="post">
        @csrf
        <label>First name: </label> <br><br>
        <input type="text" name="firstname"> <br><br>
        <label>Last name: </label> <br><br>
        <input type="text" name="lastname"> <br><br>
        <label>Gender: </label> <br><br>
        <input type="radio" name="gender" value="Male">Male <br>
        <input type="radio" name="gender" value="Female">Female <br>
        <input type="radio" name="gender" value="Other">Other <br>
        <br>
        <label>Nationality:</label><br><br>
        <select name="nationality" id="">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="Australian">Australian</option>
        </select>
        <br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br>
        <br>
        <label>Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="13"></textarea><br>
        <input type="submit" value="Sign Up" name="btnsignup">
    </form>
@endsection