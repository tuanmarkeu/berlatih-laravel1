@extends('layout.master')
@section('judul')
    Halaman Tambah Cast
@endsection
@section('content')
    <form method="post" action="/masukin">
        @csrf
        <div class="form-group">
            <label class="form-label">Nama : </label>
            <input type="text" class="form-control" name="inputnama">
        </div>
        @error('inputnama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label class="form-label">Umur : </label>
            <input type="text" class="form-control" name="inputumur">
        </div>
        @error('inputumur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label class="form-label">Bio : </label>
            <textarea name="inputbio" id="" cols="10" rows="10" class="form-control"></textarea>
        </div>
        @error('inputbio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection